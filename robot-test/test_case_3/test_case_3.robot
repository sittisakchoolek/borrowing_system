*** Settings ***
Documentation    0001 0009 0012 0013
Library     SeleniumLibrary
Library     OperatingSystem

*** Variables ***
${เว็บ}                 https://borrowing2024.000webhostapp.com/
${BROWSER}               Firefox
${btnSelectWordfile}     xpath=//input[@type="file"]
${imgFile}               ${CURDIR}${/}testfile.jpg

*** Test Cases ***
Login Test
    Open The Browser
    Maximize The Browser
    Enter Username And Password
    Waiting     2s
    Click To Login
    Waiting     2s
    Click Link
    Waiting     2s
    # Click Insert
    # Insert Data    โค้ดถูกแล้ว แต่datatable มันเอ๋อ
    # Waiting     4s
    # Modify Data   โค้ดถูกแล้ว แต่datatable มันเอ๋อ
    # Waiting     2s
    Delete Data
    Waiting     2s
    Close The Browser

*** Keywords ***
Open The Browser
    Open Browser     ${เว็บ}    ${BROWSER}
    Wait Until Element Is Visible    id:username

Maximize The Browser
    Maximize Browser Window

Enter Username And Password
    Input Text     id:username    admin
    Input Text     id:password    admin

Click To Login
    Click Element    css=button[type='submit']

Click Link
    Click Element    css:a[href="./ins_equipment.html"]

# Click Insert
#     Click Button    id:add_equipment_btn
#     Waiting     3s

# Insert Data
#     Input Text    id:equipmentId    30
#     Input Text    id:equipmentName    omnitrix
#     Input Text    id:invenQty    69
#     Select From List By Value    id:permission    1
#     Select From List By Value    id:categoryId    13   #!! datatable มันเอ๋อไม่ยอมเรียกฐาณข้อมูลมาก่อน เลยเกิด error !!
#     Choose File    id:equipmentImg    ${imgFile}
#     Click Element    id:action
#     Waiting     2s
#     Click Element    css=button.swal2-confirm

# Modify Data
#     Click Button    id:update-equip-30
#     Select From List By Value    id:permission    2
#     Waiting     2s
#     Click Element    id:action

Delete Data
    # Click Element    xpath=//a[@class="page-link" and text()="2"]
    Waiting     2s
    Click Button    id:delete-equip-30
    Waiting     2s
    Click Element    css=button.swal2-confirm.swal2-styled.swal2-default-outline
    Waiting     3s
    Click Element    css=button.swal2-confirm
    
Waiting 
    [Arguments]     ${time}
    Sleep     ${time}

Close The Browser
    Close Browser
