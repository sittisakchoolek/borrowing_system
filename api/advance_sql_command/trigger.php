<?php
include("./../asset/conn.php");

// Create a trigger to update inventory when a reservation is made
$queryCreateTriggerReserve = "
CREATE TRIGGER reserve_trigger
AFTER INSERT ON selected_reserve
FOR EACH ROW
BEGIN
    DECLARE update_qty INT;
    SELECT invenQty INTO update_qty FROM equipment_list WHERE equipmentId = NEW.equipmentId;
    SET update_qty = update_qty - NEW.reserveQty;
    UPDATE equipment_list SET invenQty = update_qty WHERE equipmentId = NEW.equipmentId;
END
";

$conn->exec($queryCreateTriggerReserve);

$queryCreateTriggerCancel = "
CREATE TRIGGER cancel_trigger
AFTER DELETE ON selected_reserve
FOR EACH ROW
BEGIN
    DECLARE reset_qty INT;
    SELECT invenQty INTO reset_qty FROM equipment_list WHERE equipmentId = OLD.equipmentId;
    SET reset_qty = reset_qty + OLD.reserveQty;
    UPDATE equipment_list SET invenQty = reset_qty WHERE equipmentId = OLD.equipmentId;
END
";

$conn->exec($queryCreateTriggerCancel);
?>
