<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    if (isset($_POST["operation"])) {
        
        if ($_POST["operation"] == "insert") {
            if (
                isset($_POST["equipmentId"]) &&
                isset($_POST["equipmentName"]) &&
                isset($_POST["invenQty"]) &&
                isset($_POST["permission"]) &&
                isset($_POST["categoryId"]) &&
                isset($_FILES["equipmentImg"])
            ) {
                // Handle file upload errors
                if ($_FILES["equipmentImg"]["size"] > 5120000) {
                    echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพที่มีขนาดเล็กกว่า 5MB</p>";
                } else {
                    $equipmentId     =   $_POST["equipmentId"];
                    $equipmentName   =   $_POST["equipmentName"];
                    $invenQty        =   $_POST["invenQty"];
                    $permission      =   $_POST["permission"];
                    $categoryId      =   $_POST["categoryId"];

                    $uploadOk = 1;
                    $fileDir = "./../../upload_files/equipment/";

                    $fileName = basename($_FILES["equipmentImg"]["name"]);
                    $imageFileType = pathinfo($_FILES["equipmentImg"]["name"], PATHINFO_EXTENSION);

                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                        echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพ .jpg .png หรือ .jpeg</p>";
                        $uploadOk = 0;
                    }

                    if ($uploadOk == 0) {
                        // die("ไม่สามารถอัพโหลดรูปภาพได้ กรุณาลองใหม่อีกครั้ง<br>");
                    } else {
                        $fileNameUpload = guidv4() . "." . $imageFileType;
                        $filePathUpload = $fileDir . $fileNameUpload;
        
                        if (is_uploaded_file($_FILES["equipmentImg"]["tmp_name"])) {
                            if (move_uploaded_file($_FILES["equipmentImg"]["tmp_name"], $filePathUpload)) {
                                
                                $insertEquipment = "INSERT INTO equipment_list 
                                (equipmentId, equipmentName, equipmentImg, invenQty, permission, categoryId) VALUES 
                                (:equipmentId, :equipmentName, :equipmentImg, :invenQty, :permission, :categoryId)";
                                
                                $stmt = $conn->prepare($insertEquipment);
                                $stmt->bindParam(':equipmentId', $equipmentId);
                                $stmt->bindParam(':equipmentName', $equipmentName);
                                $stmt->bindParam(':equipmentImg', $fileNameUpload);
                                $stmt->bindParam(':invenQty', $invenQty);
                                $stmt->bindParam(':permission', $permission);
                                $stmt->bindParam(':categoryId', $categoryId);

                                // $p_insertEquipment = 
                                // "SELECT InsertUpdateEquipment (:equipmentId, :equipmentName, :equipmentImg, :invenQty, :permission, :categoryId, 'insert')";
                                
                                // $stmt = $conn->prepare($p_insertEquipment);
                                // $stmt->bindParam(':equipmentId', $equipmentId);
                                // $stmt->bindParam(':equipmentName', $equipmentName);
                                // $stmt->bindParam(':equipmentImg', $fileNameUpload);
                                // $stmt->bindParam(':invenQty', $invenQty);
                                // $stmt->bindParam(':permission', $permission);
                                // $stmt->bindParam(':categoryId', $categoryId);
                                
                                if ($stmt->execute()) {
                                    echo "<p class='text-success'>เพิ่มข้อมูลเรียบร้อยแล้ว</p>";
                                    echo "<img src='$filePathUpload' alt='ไฟล์รูป $fileNameUpload' height='420'>";
                                } else {
                                    echo "<p class='text-danger'>มีปัญหาในการบันทึกข้อมูลลงฐานข้อมูล</p>";
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($_POST["operation"] == "update") {
            if (
                isset($_POST["equipmentId"]) &&
                isset($_POST["equipmentName"]) &&
                isset($_POST["invenQty"]) &&
                isset($_POST["permission"]) &&
                isset($_POST["categoryId"])
            ) {
                $equipmentId     =   $_POST["equipmentId"];
                $equipmentName   =   $_POST["equipmentName"];
                $invenQty        =   $_POST["invenQty"];
                $permission      =   $_POST["permission"];
                $categoryId      =   $_POST["categoryId"];

                $uploadOk = 1;
                $fileDir = "./../../upload_files/equipment/";

                // Check if a new image file is uploaded
                if (!empty($_FILES["equipmentImg"]["tmp_name"])) {
                    // Handle file upload
                    if ($_FILES["equipmentImg"]["size"] > 5120000) {
                        echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพที่มีขนาดเล็กกว่า 5MB</p>";
                    } else {
                        $fileName = basename($_FILES["equipmentImg"]["name"]);
                        $imageFileType = pathinfo($_FILES["equipmentImg"]["name"], PATHINFO_EXTENSION);

                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                            echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพ .jpg .png หรือ .jpeg</p>";
                        } else {
                            // Upload new image file
                            $fileNameUpload = guidv4() . "." . $imageFileType;
                            $filePathUpload = $fileDir . $fileNameUpload;
                            if (move_uploaded_file($_FILES["equipmentImg"]["tmp_name"], $filePathUpload)) {
                                // Update database with new image file and other fields
                                
                                // $updateEquipment = 
                                // "UPDATE equipment_list SET 
                                // equipmentName = :equipmentName, invenQty = :invenQty, permission = :permission, 
                                // categoryId = :categoryId, equipmentImg = :equipmentImg 
                                // WHERE equipmentId = :equipmentId";
                                
                                // $stmt = $conn->prepare($updateEquipment);
                                // $stmt->bindParam(':equipmentId', $equipmentId);
                                // $stmt->bindParam(':equipmentName', $equipmentName);
                                // $stmt->bindParam(':invenQty', $invenQty);
                                // $stmt->bindParam(':permission', $permission);
                                // $stmt->bindParam(':categoryId', $categoryId);
                                // $stmt->bindParam(':equipmentImg', $fileNameUpload);

                                $p_updateEquipment = 
                                "SELECT InsertUpdateEquipment (:equipmentId, :equipmentName, :equipmentImg, :invenQty, :permission, :categoryId, 'update')";

                                $stmt = $conn->prepare($p_updateEquipment);
                                $stmt->bindParam(':equipmentId', $equipmentId);
                                $stmt->bindParam(':equipmentName', $equipmentName);
                                $stmt->bindParam(':invenQty', $invenQty);
                                $stmt->bindParam(':permission', $permission);
                                $stmt->bindParam(':categoryId', $categoryId);
                                $stmt->bindParam(':equipmentImg', $fileNameUpload);
                                if ($stmt->execute()) {
                                    echo "<p class='text-success'>อัพเดตข้อมูลเรียบร้อยแล้ว</p>";
                                    // echo "<img src='$filePathUpload' alt='ไฟล์รูป $fileNameUpload' height='420'>";
                                } else {
                                    echo "<p class='text-danger'>มีปัญหาในการอัพเดตข้อมูลลงฐานข้อมูล</p>";
                                }
                            }
                        }
                    }
                } else {
                    // Update database without changing image file
                    $updateEquipment = 
                    "UPDATE equipment_list SET 
                    equipmentName = :equipmentName, invenQty = :invenQty, 
                    permission = :permission, categoryId = :categoryId
                    WHERE equipmentId = :equipmentId";
                    $stmt = $conn->prepare($updateEquipment);
                    $stmt->bindParam(':equipmentId', $equipmentId);
                    $stmt->bindParam(':equipmentName', $equipmentName);
                    $stmt->bindParam(':invenQty', $invenQty);
                    $stmt->bindParam(':permission', $permission);
                    $stmt->bindParam(':categoryId', $categoryId);
                    if ($stmt->execute()) {
                        echo "<p class='text-success'>อัพเดตข้อมูลเรียบร้อยแล้ว</p>";
                    } else {
                        echo "<p class='text-danger'>มีปัญหาในการอัพเดตข้อมูลลงฐานข้อมูล</p>";
                    }
                }
            }
        }

        if ($_POST["operation"] == "delete") {
            if (isset($_POST["manageId"])) {
                $manageId = extractNumber($_POST["manageId"]);
                $queryDeleteRow = 
                "DELETE FROM equipment_list WHERE equipmentId = :equipmentId";
                $stmt = $conn->prepare($queryDeleteRow);
                $stmt->bindParam(':equipmentId', $manageId);
                $stmt->execute(
                    // array(':equipmentId' => $manageId)
                );
            }
        }
    }

    $conn = null;
?>