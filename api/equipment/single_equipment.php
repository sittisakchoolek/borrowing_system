<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");
    
    if(isset($_POST["manageId"])) {
        $manageId = extractNumber($_POST["manageId"]);
        $output = array();
        
        $querySingleEquipment = 
        "SELECT * FROM category JOIN equipment_list USING(categoryId) 
        WHERE equipmentId = :manageId LIMIT 1";
        
        $stmt = $conn->prepare($querySingleEquipment);
        $stmt->bindParam(':manageId', $manageId);
        $stmt->execute();
        $result = $stmt->fetchAll();

        // $stmt = $conn->prepare("CALL GetEquipment(:manageId)");
        // $stmt->bindParam(':manageId', $manageId);
        // $stmt->execute();
        // $result = $stmt->fetchAll();

        foreach($result as $row) {
            $output["equipmentId"]     =   $row["equipmentId"];
            $output["equipmentName"]   =   $row["equipmentName"];
            $output["equipmentImg"]    =   $row["equipmentImg"];
            $output["invenQty"]        =   $row["invenQty"];
            $output["permission"]      =   $row["permission"];
            $output["categoryId"]      =   $row["categoryId"];
            $output["categoryName"]    =   $row["categoryName"];
        }
        
        echo json_encode($output);
    }
?>