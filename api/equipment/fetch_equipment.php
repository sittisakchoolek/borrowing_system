<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    $queryEquipment = '';
    $output = array();
    
    if (isset($_POST["userStatus"])) {
        // Fetch specific equipment data with permission
        $userStatus = $_POST["userStatus"];
        
        switch ($userStatus) {
            // Equipment for students
            case 1:
                $queryEquipment .= 
                "SELECT * FROM category JOIN equipment_list USING(categoryId)
                WHERE permission = 1 ORDER BY equipmentId";
                break;
            // Equipment for staffs
            case 2:
                $queryEquipment .= 
                "SELECT * FROM category JOIN equipment_list USING(categoryId)
                WHERE permission IN (1, 2) ORDER BY equipmentId";
                break;
            // Equipment for officer
            case 3:
                $queryEquipment .= 
                "SELECT * FROM category JOIN equipment_list USING(categoryId)
                WHERE permission IN (1, 2, 3) ORDER BY equipmentId";
                break;
        }
    } else {
        // Fetch all equipment data by default
        $queryEquipment .= 
        "SELECT * FROM category JOIN equipment_list USING(categoryId)";
    }
    
    $stmt = $conn->prepare($queryEquipment);
    $stmt->execute();
    $dataResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $data = array();
    $filteredRows = $stmt->rowCount();

    foreach($dataResult as $row) {
        $equipmentItem = array();

        $equipmentItem['equipmentId']   =   $row["equipmentId"];
        $equipmentItem['equipmentName'] =   $row["equipmentName"];
        $equipmentItem['equipmentImg']  =   $row["equipmentImg"];
        $equipmentItem['invenQty']      =   $row["invenQty"];
        $equipmentItem['permission']    =   $row["permission"];
        $equipmentItem['categoryName']  =   $row["categoryName"];
        $equipmentItem['measure']       =   $row["measure"];
        $equipmentItem['update']        =   $row["equipmentId"];
        $equipmentItem['delete']        =   $row["equipmentId"];
    
        $data[] = $equipmentItem;
    }

    $conn = null;

    $output = array(
        "draw" => 1,
        "recordsTotal" => $filteredRows,
        "recordsFiltered" => getAllRecords("category"),
        "data" => $data
    );
    
    echo json_encode($output);
?>