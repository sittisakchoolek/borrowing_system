<?php
    function getAllRecords($relation) {
        include('./../asset/conn.php');
        $readQuery = "SELECT * FROM $relation";
        $stmt = $conn->prepare($readQuery);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $stmt->rowCount();
    }
    
    function guidv4($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
    
        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    
        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    function psupassportAuthentication($username, $password) {
        $params = array('username' => "$username", 'password' => "$password");

        // Config php.ini on xampp control panel at 'Config'
        // remove ; at extension=soap to use soap api

        $url = "https://passport.psu.ac.th/authentication/authentication.asmx?WSDL";
        $client = new SoapClient($url, array("trace" => 1, "exceptions" => 0, "cache_wsdl" => 0));
        $result = $client->GetUserDetails($params);
        $authen_data = [];
        if (!is_a($result, "SoapFault")) {
            $authen_data = [
                "status" => true,
                "result" => $result->GetUserDetailsResult->string,
            ];
        } else {
            $authen_data = [
                "status" => false,
                "result" => $result->faultstring,
            ];
        }
        return $authen_data;
    }

    function getPermissionContent($text) {
        $pattern = '/OU=Students/';
        
        if (preg_match($pattern, $text, $matches)) {
            return $matches[0];
        } else {
            return null;
        }
    }

    function extractNumber($string) {
        preg_match('/\d+/', $string, $matches);
        return isset($matches[0]) ? $matches[0] : null;
    }
?>