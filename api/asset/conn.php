<?php
    $host = "localhost";
    $dbName = "borrowing_system";
    $userName = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dbName", $userName, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connected Successfully";
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }

    date_default_timezone_set('Asia/Bangkok');
?>