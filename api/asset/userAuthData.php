<?php
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    
    header('Content-Type: application/json');
    
    if (
        isset($_SESSION['userId']) &&
        isset($_SESSION['userName']) &&
        isset($_SESSION['userStatus'])
    ) {
        echo json_encode(
            array(
                'userId' => $_SESSION['userId'],
                'userName' => $_SESSION['userName'],
                'userStatus' => $_SESSION['userStatus']
            ),
            JSON_UNESCAPED_UNICODE
        );
    }
?>