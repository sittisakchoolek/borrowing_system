<?php
// session_start();
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
header("Cache-control: private"); // IE 6 Fix. when you click your back button
//ตรวจสอบว่าผ่าน login เข้าระบบมาแล้ว
if(isset($_SESSION['userId'])) {
	unset($_SESSION['userId']);
	unset($_SESSION['userName']);
	unset($_SESSION['userStatus']);
	$_SESSION = array(); // reset session array
	session_destroy();   // Finally, destroy the session.
	
	// echo "<b>You have successfully logged out<b><br>"; 
	// echo "<a href=\"s_index.html\">Login again</a>";
	
	header("Location: http://localhost/php2566/borrowing_system/login.html");
}
else { 
	// echo "<b>Error : You do not login to system<b><br>";
	// echo "Please <a href=\"s_index.html\">login</a><br>";
	
	header("Location: http://localhost/php2566/borrowing_system/login.html");
}
?>