<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    $queryReturnData = "";
    $output = array();
    
    $queryReturnData .= 
    "SELECT * FROM borrowing_detail NATURAL JOIN borrowing
    NATURAL JOIN equipment_list NATURAL JOIN category
    WHERE returnStatus IN ('no', 'yes') ORDER BY borrowId DESC";

    $stmt = $conn->prepare($queryReturnData);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $data = array();
    $filteredRows = $stmt->rowCount();

    foreach($result as $row) {
        $borrowList = array();
        
        $borrowList["borrowId"]         =       $row["borrowId"];
        $borrowList["userId"]           =       $row["userId"];
        $borrowList["equipmentId"]      =       $row["equipmentId"];
        $borrowList["name"]             =       $row["name"];
        $borrowList["title"]            =       $row["title"];
        $borrowList["telNumber"]        =       $row["telNumber"];
        $borrowList["objective"]        =       $row["objective"];
        $borrowList["borrowDate"]       =       $row["borrowDate"];
        $borrowList["returnDate"]       =       $row["returnDate"];
        $borrowList["period"]           =       $row["period"];
        $borrowList["docFile"]          =       $row["docFile"];
        $borrowList["borrowQty"]        =       $row["borrowQty"];
        $borrowList["approveStatus"]    =       $row["approveStatus"];
        $borrowList["returnStatus"]     =       $row["returnStatus"];
        $borrowList["damageQty"]        =       $row["damageQty"];
        $borrowList["notation"]         =       $row["notation"];
        $borrowList["equipmentName"]    =       $row["equipmentName"];
        $borrowList["equipmentImg"]     =       $row["equipmentImg"];
        $borrowList["invenQty"]         =       $row["invenQty"];
        $borrowList["categoryName"]     =       $row["categoryName"];
        $borrowList["measure"]          =       $row["measure"];
        $borrowList["return"]           =       "return-" . $row["borrowId"] . "-" . $row["equipmentId"];
        $borrowList["delete"]           =       "delete-" . $row["borrowId"] . "-" . $row["equipmentId"];

        $data[] = $borrowList;
    }

    $conn = null;

    header('Content-Type: application/json');
    $output = array(
        "draw" => 1,
        "recordsTotal" => $filteredRows,
        "recordsFiltered" => getAllRecords("borrowing_detail"),
        "data" => $data
    );

    echo json_encode($output);
?>