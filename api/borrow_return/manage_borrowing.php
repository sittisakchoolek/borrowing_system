<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    if (isset($_POST["operation"])) {
        
        if ($_POST["operation"] == "insert") {
            $brrId;
            
            // Insert to borrowing relation
            if (
                isset($_POST["userId"]) &&
                isset($_POST["name"]) &&
                isset($_POST["title"]) &&
                isset($_POST["telNumber"]) &&
                isset($_POST["borrowDate"]) &&
                isset($_POST["returnDate"]) &&
                isset($_POST["objective"]) &&
                isset($_POST["reserveSeries"])
            ) {
                // Insert values to borrowing and borrowing_detail relation
                $userId         =       $_POST["userId"];
                $name           =       $_POST["name"];
                $title          =       $_POST["title"];
                $telNumber      =       $_POST["telNumber"];
                $objective      =       $_POST["objective"];
                $equipmentId    =       '';
                $reserveQty     =       '';
                
                $borrowDateInput     =       DateTime::createFromFormat('Y-m-d', $_POST["borrowDate"]);
                $returnDateInput     =       DateTime::createFromFormat('Y-m-d', $_POST["returnDate"]);
                if (!$borrowDateInput || !$returnDateInput) {
                    // Handle error, for example:
                    echo "Invalid date format.";
                    exit; // Stop script execution
                }
                $borrowDate = $borrowDateInput->format('Y-m-d');
                $returnDate = $returnDateInput->format('Y-m-d');
                $diff = $borrowDateInput->diff($returnDateInput);
                $period = $diff->days + 1;
                
                $uploadOk = 1;
                $fileDir = "./../../upload_files/document/";
    
                if (!empty($_FILES["fileUploadPdf"]["tmp_name"])) {
                    $fileName = basename($_FILES["fileUploadPdf"]["name"]);
                    $docsFileType = pathinfo($_FILES["fileUploadPdf"]["name"], PATHINFO_EXTENSION);
    
                    if ($docsFileType != "pdf") {
                        echo "<p class='text-danger'>กรุณาเลือกไฟล์เอกสาร .pdf</p>";
                    } else {
                        $fileNameUpload = guidv4() . "." . $docsFileType;
                        $filePathUpload = $fileDir . $fileNameUpload;
                        if (move_uploaded_file($_FILES["fileUploadPdf"]["tmp_name"], $filePathUpload)) {
                            
                            // Insert into borrowing relation
                            $insertBorrowing = 
                            "INSERT INTO borrowing (name, title, telNumber, objective, 
                            borrowDate, returnDate, period, docFile, userId) 
                            VALUES (:name, :title, :telNumber, :objective, 
                            :borrowDate, :returnDate, :period, :docFile, :userId)";
                            $stmt = $conn->prepare($insertBorrowing);
                            $stmt->bindParam(':name', $name);
                            $stmt->bindParam(':title', $title);
                            $stmt->bindParam(':telNumber', $telNumber);
                            $stmt->bindParam(':objective', $objective);
                            $stmt->bindParam(':borrowDate', $borrowDate);
                            $stmt->bindParam(':returnDate', $returnDate);
                            $stmt->bindParam(':period', $period);
                            $stmt->bindParam(':docFile', $fileNameUpload);
                            $stmt->bindParam(':userId', $userId);
                            $stmt->execute();
    
                            // Get borrow id
                            $queryBorrowId = 
                            "SELECT * FROM borrowing
                            WHERE userId = :userId AND telNumber = :telNumber 
                            AND borrowDate = :borrowDate AND returnDate = :returnDate";
                            $stmt = $conn->prepare($queryBorrowId);
                            $stmt->bindParam(':userId', $userId);
                            $stmt->bindParam(':telNumber', $telNumber);
                            $stmt->bindParam(':borrowDate', $borrowDate);
                            $stmt->bindParam(':returnDate', $returnDate);
                            $stmt->execute();
                            
                            $selectedBorrowing = $stmt->fetchAll();
                            foreach ($selectedBorrowing as $row) {
                                $brrId = $row["borrowId"];
                            }
                
                            // Query selected_reserve relation and Insert into borrowing_detail relation
                            $querySelectedReserve = 
                            "SELECT * FROM selected_reserve WHERE series = :series";
                            
                            // Assuming $_POST["reserveSeries"] is an array of series
                            foreach ($_POST["reserveSeries"] as $key => $series) {
                                $stmt = $conn->prepare($querySelectedReserve);
                                $stmt->bindParam(':series', $series);
                                $stmt->execute();
                                
                                $selectedReserve = $stmt->fetchAll();
                                foreach ($selectedReserve as $row) {
                                    $equipmentId = $row["equipmentId"];
                                    $reserveQty = $row["reserveQty"];
                                }

                                $insertBorrowingDetail =
                                "INSERT INTO borrowing_detail (borrowId, equipmentId, borrowQty)
                                VALUES (:borrowId, :equipmentId, :borrowQty)";
                                $stmt = $conn->prepare($insertBorrowingDetail);
                                $stmt->bindParam(':borrowId', $brrId);
                                $stmt->bindParam(':equipmentId', $equipmentId);
                                $stmt->bindParam(':borrowQty', $reserveQty);
                                $stmt->execute();
                            }
                        }
                    }
                } else {
                    // Insert into borrowing relation
                    $insertBorrowing = 
                    "INSERT INTO borrowing (name, title, telNumber, objective, 
                    borrowDate, returnDate, period, userId) 
                    VALUES (:name, :title, :telNumber, :objective, 
                    :borrowDate, :returnDate, :period, :userId)";
                    $stmt = $conn->prepare($insertBorrowing);
                    $stmt->bindParam(':name', $name);
                    $stmt->bindParam(':title', $title);
                    $stmt->bindParam(':telNumber', $telNumber);
                    $stmt->bindParam(':objective', $objective);
                    $stmt->bindParam(':borrowDate', $borrowDate);
                    $stmt->bindParam(':returnDate', $returnDate);
                    $stmt->bindParam(':period', $period);
                    $stmt->bindParam(':userId', $userId);
                    $stmt->execute();
    
                    // Get borrow id
                    $queryBorrowId = 
                    "SELECT * FROM borrowing
                    WHERE userId = :userId AND telNumber = :telNumber 
                    AND borrowDate = :borrowDate AND returnDate = :returnDate";
                    $stmt = $conn->prepare($queryBorrowId);
                    $stmt->bindParam(':userId', $userId);
                    $stmt->bindParam(':telNumber', $telNumber);
                    $stmt->bindParam(':borrowDate', $borrowDate);
                    $stmt->bindParam(':returnDate', $returnDate);
                    $stmt->execute();
                    
                    $selectedBorrowing = $stmt->fetchAll();
                    foreach ($selectedBorrowing as $row) {
                        $brrId = $row["borrowId"];
                    }

                    // Query selected_reserve relation and Insert into borrowing_detail relation
                    $querySelectedReserve = 
                    "SELECT * FROM selected_reserve WHERE series = :series";
                    
                    // Assuming $_POST["reserveSeries"] is an array of series
                    foreach ($_POST["reserveSeries"] as $key => $series) {
                        $stmt = $conn->prepare($querySelectedReserve);
                        $stmt->bindParam(':series', $series);
                        $stmt->execute();
                        
                        $selectedReserve = $stmt->fetchAll();
                        foreach ($selectedReserve as $row) {
                            $equipmentId = $row["equipmentId"];
                            $reserveQty = $row["reserveQty"];
                        }

                        $insertBorrowingDetail =
                        "INSERT INTO borrowing_detail (borrowId, equipmentId, borrowQty)
                        VALUES (:borrowId, :equipmentId, :borrowQty)";
                        $stmt = $conn->prepare($insertBorrowingDetail);
                        $stmt->bindParam(':borrowId', $brrId);
                        $stmt->bindParam(':equipmentId', $equipmentId);
                        $stmt->bindParam(':borrowQty', $reserveQty);
                        $stmt->execute();
                    }
                }
            }
        }

        if ($_POST["operation"] == "approve") {
            if (isset($_POST["borrowId"]) && isset($_POST["equipmentId"])) {
                $queryApprove = 
                "UPDATE borrowing_detail 
                SET approveStatus = 'yes', returnStatus = 'no'
                WHERE borrowId = :borrowId AND equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($queryApprove);
                $stmt->bindParam(':borrowId', $_POST["borrowId"]);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
            }
        }

        if ($_POST["operation"] == "notApprove") {
            if (isset($_POST["borrowId"]) && isset($_POST["equipmentId"])) {
                
                // Select borrow quantity
                $querySelectBorrowQty = 
                "SELECT * FROM borrowing_detail
                WHERE borrowId = :borrowId AND equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($querySelectBorrowQty);
                $stmt->bindParam(':borrowId', $_POST["borrowId"]);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
                
                $selectBorrowQty = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $borrowQty;
                foreach ($selectBorrowQty as $row) {
                    $borrowQty = $row['borrowQty'];
                }

                // Select inventory quantity
                $querySelectInvenQty = 
                "SELECT * FROM equipment_list
                WHERE equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($querySelectInvenQty);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
                
                $selectInvenQty = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $invenQty;
                foreach ($selectInvenQty as $row) {
                    $invenQty = $row['invenQty'];
                }
                
                $resetInvenQty = $invenQty + $borrowQty;
                
                // Reset inventory quantity
                $queryResetInvenQty = 
                "UPDATE equipment_list SET invenQty = :resetInvenQty 
                WHERE equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($queryResetInvenQty);
                $stmt->bindParam(':resetInvenQty', $resetInvenQty);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
                
                // Update borrow status
                $queryNotApprove = 
                "UPDATE borrowing_detail 
                SET approveStatus = 'no', returnStatus = 'null'
                WHERE borrowId = :borrowId AND equipmentId = :equipmentId";
                $stmt = $conn->prepare($queryNotApprove);
                $stmt->bindParam(':borrowId', $_POST["borrowId"]);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
            }
        }

        if ($_POST["operation"] == "return") {
            if (isset($_POST["borrowId"]) && isset($_POST["equipmentId"])) {
                
                // Select inventory quantity
                $querySelectInvenQty = 
                "SELECT * FROM equipment_list
                WHERE equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($querySelectInvenQty);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
                
                $selectInvenQty = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $invenQty;
                foreach ($selectInvenQty as $row) {
                    $invenQty = $row['invenQty'];
                }
                
                // Select borrow quantity
                $querySelectBorrowQty = 
                "SELECT * FROM borrowing_detail
                WHERE borrowId = :borrowId AND equipmentId = :equipmentId";

                $stmt = $conn->prepare($querySelectBorrowQty);
                $stmt->bindParam(':borrowId', $_POST["borrowId"]);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();

                $selectBorrowQty = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $borrowQty;
                foreach ($selectBorrowQty as $row) {
                    $borrowQty = $row['borrowQty'];
                }

                // Update inventory quqntity
                $updateInvenQty = $borrowQty + $invenQty;

                $queryUpdateInvenQty = 
                "UPDATE equipment_list SET invenQty = :updateInvenQty
                WHERE equipmentId = :equipmentId";
                
                $stmt = $conn->prepare($queryUpdateInvenQty);
                $stmt->bindParam(':updateInvenQty', $updateInvenQty);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();

                // Update user return status
                $queryUpdateReturnStatus = 
                "UPDATE borrowing_detail SET returnStatus = 'yes'
                WHERE borrowId = :borrowId AND equipmentId = :equipmentId";

                $stmt = $conn->prepare($queryUpdateReturnStatus);
                $stmt->bindParam(':borrowId', $_POST["borrowId"]);
                $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
                $stmt->execute();
            }
        }
    }
?>