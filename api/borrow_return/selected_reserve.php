<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");
    
    if (
        isset($_POST["equipmentId"]) && 
        isset($_POST["reserveQty"]) && 
        isset($_POST["userId"]) 
    ) {
        $querySelectedEquip = 
        "SELECT * FROM equipment_list 
        WHERE equipmentId = :equipmentId LIMIT 1";
        
        $stmt = $conn->prepare($querySelectedEquip);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->execute();
        
        $selectedEquip = $stmt->fetchAll();
        foreach ($selectedEquip as $row) {
            $invenQty = $row["invenQty"];
        }
        
        $updateInvenQty = $invenQty - $_POST["reserveQty"];
        $queryUpdateInvenQty = 
        "UPDATE equipment_list SET invenQty = :updateInvenQty 
        WHERE equipmentId = :equipmentId";
        
        $stmt = $conn->prepare($queryUpdateInvenQty);
        $stmt->bindParam(':updateInvenQty', $updateInvenQty);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->execute();
        
        $queryReserve = 
        "INSERT INTO selected_reserve (equipmentId, reserveQty, userId)
        VALUES (:equipmentId, :reserveQty, :userId)";
        
        $stmt = $conn->prepare($queryReserve);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->bindParam(':reserveQty', $_POST["reserveQty"]);
        $stmt->bindParam(':userId', $_POST["userId"]);
        $stmt->execute();

        $queryReserveResult =
        "SELECT * FROM equipment_list 
        NATURAL JOIN category NATURAL JOIN selected_reserve
        WHERE equipmentId = :equipmentId AND userId = :userId";
        
        $stmt = $conn->prepare($queryReserveResult);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->bindParam(':userId', $_POST["userId"]);
        $stmt->execute();
        $selectedEquipReserve = $stmt->fetchAll();

        $reserveResult = array();

        foreach ($selectedEquipReserve as $row) {
            $reserveResult["equipmentId"]     =   $row["equipmentId"];
            $reserveResult["equipmentName"]   =   $row["equipmentName"];
            $reserveResult["equipmentImg"]    =   $row["equipmentImg"];
            $reserveResult["invenQty"]        =   $row["invenQty"];
            $reserveResult["reserveQty"]      =   $row["reserveQty"];
            $reserveResult["measure"]         =   $row["measure"];
            $reserveResult["cancel"]          =   $row["series"];
        }

        echo json_encode($reserveResult, JSON_UNESCAPED_UNICODE);
    }

    if (
        isset($_POST["equipmentId"]) && 
        isset($_POST["cancel"]) && 
        isset($_POST["reserveQty"])
    ) {
        $queryPresentData = 
        "SELECT * FROM equipment_list WHERE equipmentId = :equipmentId LIMIT 1";
        $stmt = $conn->prepare($queryPresentData);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->execute();
        
        $presentInvenQty = $stmt->fetchAll();
        foreach ($presentInvenQty as $row) {
            $invenQty = $row["invenQty"];
        }
        
        $resetInvenQty = $invenQty + $_POST["reserveQty"];
        
        $queryResetInvenQty = 
        "UPDATE equipment_list SET invenQty = :resetInvenQty
        WHERE equipmentId = :equipmentId";
        $stmt = $conn->prepare($queryResetInvenQty);
        $stmt->bindParam(':resetInvenQty', $resetInvenQty);
        $stmt->bindParam(':equipmentId', $_POST["equipmentId"]);
        $stmt->execute();
        
        $queryCancelReserve = "DELETE FROM selected_reserve WHERE series = :cancel";
        $stmt = $conn->prepare($queryCancelReserve);
        $stmt->bindParam(':cancel', $_POST["cancel"]);
        $stmt->execute();
    }
    
    $conn = null;
?>