<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    $queryBorrowData = "";
    $output = array();
    
    $queryBorrowData .= 
    "SELECT * FROM borrowing NATURAL JOIN borrowing_detail 
    NATURAL JOIN equipment_list NATURAL JOIN category ORDER BY borrowId DESC";

    // $borrowStatus = $_POST["borrowStatus"];
    // $startDate = $_POST["startDate"];
    // $endDate = $_POST["endDate"];

    // if (!empty($borrowStatus) && empty($startDate) && empty($endDate)) {
        
    //     $queryBorrowData .= "WHERE approveStatus = :borrowStatus";
    //     $stmt = $conn->prepare($queryBorrowData);
    //     $stmt->bindParam(':borrowStatus', $borrowStatus);
    //     $stmt->execute();
        
    // }
    
    // if (empty($borrowStatus) && !empty($startDate) && !empty($endDate)) {
        
    //     $queryBorrowData .= "WHERE borrowDate BETWEEN :startDate AND :endDate";
    //     $stmt = $conn->prepare($queryBorrowData);
    //     $stmt->bindParam(':startDate', $startDate);
    //     $stmt->bindParam(':endDate', $endDate);
    //     $stmt->execute();
        
    // }
    
    // if (!empty($borrowStatus) && !empty($startDate) && !empty($endDate)) {
        
    //     $queryBorrowData .= 
    //     "WHERE approveStatus = :borrowStatus
    //     AND borrowDate BETWEEN :startDate AND :endDate";
    //     $stmt = $conn->prepare($queryBorrowData);
    //     $stmt->bindParam(':borrowStatus', $borrowStatus);
    //     $stmt->bindParam(':startDate', $startDate);
    //     $stmt->bindParam(':endDate', $endDate);
    //     $stmt->execute();
        
    // }

    $stmt = $conn->prepare($queryBorrowData);
    $stmt->execute();

    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $data = array();
    $filteredRows = $stmt->rowCount();

    foreach($result as $row) {
        $borrowList = array();
        
        $borrowList["borrowId"]         =       $row["borrowId"];
        $borrowList["userId"]           =       $row["userId"];
        $borrowList["equipmentId"]      =       $row["equipmentId"];
        $borrowList["name"]             =       $row["name"];
        $borrowList["title"]            =       $row["title"];
        $borrowList["telNumber"]        =       $row["telNumber"];
        $borrowList["objective"]        =       $row["objective"];
        $borrowList["borrowDate"]       =       $row["borrowDate"];
        $borrowList["returnDate"]       =       $row["returnDate"];
        $borrowList["period"]           =       $row["period"];
        $borrowList["docFile"]          =       $row["docFile"];
        $borrowList["borrowQty"]        =       $row["borrowQty"];
        $borrowList["approveStatus"]    =       $row["approveStatus"];
        $borrowList["returnStatus"]     =       $row["returnStatus"];
        $borrowList["damageQty"]        =       $row["damageQty"];
        $borrowList["notation"]         =       $row["notation"];
        $borrowList["equipmentName"]    =       $row["equipmentName"];
        $borrowList["equipmentImg"]     =       $row["equipmentImg"];
        $borrowList["invenQty"]         =       $row["invenQty"];
        $borrowList["categoryName"]     =       $row["categoryName"];
        $borrowList["measure"]          =       $row["measure"];
        $borrowList["detail"]           =       "detail-" . $row["borrowId"] . "-" . $row["equipmentId"];
        $borrowList["approve"]          =       "approve-" . $row["borrowId"] . "-" . $row["equipmentId"];
        $borrowList["notApprove"]       =       "notApprove-" . $row["borrowId"] . "-" . $row["equipmentId"];
        $borrowList["return"]           =       "return-" . $row["borrowId"] . "-" . $row["equipmentId"];
        $borrowList["delete"]           =       "delete-" . $row["borrowId"] . "-" . $row["equipmentId"];
    
        $data[] = $borrowList;
    }
    
    $conn = null;

    header('Content-Type: application/json');
    $output = array(
        "draw" => 1,
        "recordsTotal" => $filteredRows,
        "recordsFiltered" => getAllRecords("borrowing_detail"),
        "data" => $data
    );

    echo json_encode($output);
?>