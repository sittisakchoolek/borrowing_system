<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    $queryCategory = '';
    $output = array();
    
    $queryCategory .= "SELECT * FROM category";

    $stmt = $conn->prepare($queryCategory);
    $stmt->execute();
    $dataResult = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    $data = array();
    $filteredRows = $stmt->rowCount();

    foreach ($dataResult as $row) {
        $categoryItem = array();

        $categoryItem['categoryId'] = $row["categoryId"];
        $categoryItem['categoryName'] = $row["categoryName"];
        $categoryItem['measure'] = $row["measure"];
        $categoryItem['categoryImg'] = $row["categoryImg"];
        $categoryItem['update'] = $row["categoryId"];
        $categoryItem['delete'] = $row["categoryId"];
    
        $data[] = $categoryItem;
    }

    $conn = null;

    $output = array(
        "draw" => 1,
        "recordsTotal" => $filteredRows,
        "recordsFiltered" => getAllRecords("category"),
        "data" => $data
    );
    
    echo json_encode($output);
?>