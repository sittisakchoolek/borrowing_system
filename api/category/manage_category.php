<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    if (isset($_POST["operation"])) {
        
        if ($_POST["operation"] == "insert") {
            if (
                isset($_POST["categoryId"]) &&
                isset($_POST["categoryName"]) &&
                isset($_POST["measure"]) &&
                isset($_FILES["categoryImg"])
            ) {
                // Handle file upload errors
                if ($_FILES["categoryImg"]["size"] > 5120000) {
                    echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพที่มีขนาดเล็กกว่า 5MB</p>";
                } else {
                    $categoryId     =   $_POST["categoryId"];
                    $categoryName   =   $_POST["categoryName"];
                    $measure        =   $_POST["measure"];
                    
                    $uploadOk = 1;
                    $fileDir = "./../../upload_files/category/";
        
                    $fileName = basename($_FILES["categoryImg"]["name"]);
                    $imageFileType = pathinfo($_FILES["categoryImg"]["name"], PATHINFO_EXTENSION);
        
                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                        echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพ .jpg .png หรือ .jpeg</p>";
                        $uploadOk = 0;
                    }
        
                    if ($uploadOk == 0) {
                        // die("ไม่สามารถอัพโหลดรูปภาพได้ กรุณาลองใหม่อีกครั้ง<br>");
                    } else {
                        $fileNameUpload = guidv4() . "." . $imageFileType;
                        $filePathUpload = $fileDir . $fileNameUpload;
        
                        if (is_uploaded_file($_FILES["categoryImg"]["tmp_name"])) {
                            if (move_uploaded_file($_FILES["categoryImg"]["tmp_name"], $filePathUpload)) {
                                
                                $insertCategory = "INSERT INTO category 
                                (categoryId, categoryName, measure, categoryImg) VALUES 
                                (:categoryId, :categoryName, :measure, :categoryImg)";
                                
                                $stmt = $conn->prepare($insertCategory);
                                $stmt->bindParam(':categoryId', $categoryId);
                                $stmt->bindParam(':categoryName', $categoryName);
                                $stmt->bindParam(':measure', $measure);
                                $stmt->bindParam(':categoryImg', $fileNameUpload);

                                // $p_insertCategory = 
                                // "SELECT InsertUpdateCategory(:categoryId, :categoryName, :measure, :categoryImg, 'insert')";

                                // $stmt = $conn->prepare($p_insertCategory);
                                // $stmt->bindParam(':categoryId', $categoryId);
                                // $stmt->bindParam(':categoryName', $categoryName);
                                // $stmt->bindParam(':measure', $measure);
                                // $stmt->bindParam(':categoryImg', $fileNameUpload);
                                
                                if ($stmt->execute()) {
                                    echo "<p class='text-success'>เพิ่มข้อมูลเรียบร้อยแล้ว</p>";
                                    echo "<img src='$filePathUpload' alt='ไฟล์รูป $fileNameUpload' height='420'>";
                                } else {
                                    echo "<p class='text-danger'>มีปัญหาในการบันทึกข้อมูลลงฐานข้อมูล</p>";
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($_POST["operation"] == "update") {
            if (isset($_POST["categoryId"]) && isset($_POST["categoryName"]) && isset($_POST["measure"])) {
                $categoryId     =   $_POST["categoryId"];
                $categoryName   =   $_POST["categoryName"];
                $measure        =   $_POST["measure"];

                $uploadOk = 1;
                $fileDir = "./../../upload_files/category/";
                
                // Check if a new image file is uploaded
                if (!empty($_FILES["categoryImg"]["tmp_name"])) {
                    // Handle file upload
                    if ($_FILES["categoryImg"]["size"] > 5120000) {
                        echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพที่มีขนาดเล็กกว่า 5MB</p>";
                    } else {
                        // Move uploaded file
                        $fileName = basename($_FILES["categoryImg"]["name"]);
                        $imageFileType = pathinfo($_FILES["categoryImg"]["name"], PATHINFO_EXTENSION);
                        
                        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                            echo "<p class='text-danger'>กรุณาเลือกไฟล์รูปภาพ .jpg .png หรือ .jpeg</p>";
                        } else {
                            // Upload new image file
                            $fileNameUpload = guidv4() . "." . $imageFileType;
                            $filePathUpload = $fileDir . $fileNameUpload;
                            if (move_uploaded_file($_FILES["categoryImg"]["tmp_name"], $filePathUpload)) {
                                // Update database with new image file and other fields
                                
                                $updateCategory = 
                                "UPDATE category SET 
                                categoryName = :categoryName, measure = :measure, categoryImg = :categoryImg 
                                WHERE categoryId = :categoryId";
                                
                                $stmt = $conn->prepare($updateCategory);
                                $stmt->bindParam(':categoryId', $categoryId);
                                $stmt->bindParam(':categoryName', $categoryName);
                                $stmt->bindParam(':measure', $measure);
                                $stmt->bindParam(':categoryImg', $fileNameUpload);

                                // $p_updateCategory =
                                // "SELECT InsertUpdateCategory(:categoryId, :categoryName, :measure, :categoryImg, 'update')";
                                
                                // $stmt = $conn->prepare($p_updateCategory);
                                // $stmt->bindParam(':categoryId', $categoryId);
                                // $stmt->bindParam(':categoryName', $categoryName);
                                // $stmt->bindParam(':measure', $measure);
                                // $stmt->bindParam(':categoryImg', $fileNameUpload);
                                
                                if ($stmt->execute()) {
                                    echo "<p class='text-success'>อัพเดตข้อมูลเรียบร้อยแล้ว</p>";
                                    // echo "<img src='$filePathUpload' alt='ไฟล์รูป $fileNameUpload' height='420'>";
                                } else {
                                    echo "<p class='text-danger'>มีปัญหาในการอัพเดตข้อมูลลงฐานข้อมูล</p>";
                                }
                            }
                        }
                    }
                } else {
                    // Update database without changing image file
                    $updateCategory = 
                    "UPDATE category SET 
                    categoryName = :categoryName, measure = :measure 
                    WHERE categoryId = :categoryId";
                    $stmt = $conn->prepare($updateCategory);
                    $stmt->bindParam(':categoryId', $categoryId);
                    $stmt->bindParam(':categoryName', $categoryName);
                    $stmt->bindParam(':measure', $measure);
                    if ($stmt->execute()) {
                        echo "<p class='text-success'>อัพเดตข้อมูลเรียบร้อยแล้ว</p>";
                    } else {
                        echo "<p class='text-danger'>มีปัญหาในการอัพเดตข้อมูลลงฐานข้อมูล</p>";
                    }
                }
            }
        }

        if ($_POST["operation"] == "delete") {
            if (isset($_POST["manageId"])) {
                $manageId = extractNumber($_POST["manageId"]);
                $queryDeleteRow = 
                "DELETE FROM category WHERE categoryId = :categoryId";
                $stmt = $conn->prepare($queryDeleteRow);
                $stmt->bindParam(':categoryId', $manageId);
                $stmt->execute(
                    // array(':categoryId' => $manageId)
                );
            }
        }
    }

    $conn = null;
?>