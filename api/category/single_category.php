<?php
    include("./../asset/conn.php");
    include("./../asset/function.php");
    
    if(isset($_POST["manageId"])) {
        $manageId = extractNumber($_POST["manageId"]);
        $output = array();
        
        $querySingleCategory = "SELECT * FROM category 
        WHERE categoryId = :manageId LIMIT 1";
        
        $stmt = $conn->prepare($querySingleCategory);
        $stmt->bindParam(':manageId', $manageId);
        $stmt->execute();
        $result = $stmt->fetchAll();

        // $stmt = $conn->prepare("CALL GetCategory(:manageId)");
        // $stmt->bindParam(':manageId', $manageId, PDO::PARAM_INT);
        // $stmt->execute();
        // $result = $stmt->fetchAll();

        foreach($result as $row) {
            $output["categoryId"]   =   $row["categoryId"];
            $output["categoryName"] =   $row["categoryName"];
            $output["measure"]      =   $row["measure"];
            $output["categoryImg"]  =   $row["categoryImg"];
        }
        
        echo json_encode($output);
    }
?>