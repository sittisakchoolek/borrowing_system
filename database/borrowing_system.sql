-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for borrowing_system
CREATE DATABASE IF NOT EXISTS `borrowing_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `borrowing_system`;

-- Dumping structure for table borrowing_system.borrowing
CREATE TABLE IF NOT EXISTS `borrowing` (
  `borrowId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(75) DEFAULT NULL,
  `telNumber` varchar(15) DEFAULT NULL,
  `objective` text DEFAULT NULL,
  `borrowDate` date DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `period` int(3) DEFAULT NULL,
  `docFile` varchar(255) DEFAULT NULL,
  `userId` varchar(20) NOT NULL,
  PRIMARY KEY (`borrowId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table borrowing_system.borrowing: ~9 rows (approximately)
INSERT IGNORE INTO `borrowing` (`borrowId`, `name`, `title`, `telNumber`, `objective`, `borrowDate`, `returnDate`, `period`, `docFile`, `userId`) VALUES
	(2, 'admin', 'งานจัดเลี้ยง', '0852222222', 'Test 15', '2024-03-24', '2024-03-25', 2, NULL, 'admin'),
	(3, 'admin', 'งานจัดเลี้ยง', '0852222222', 'Test 16', '2024-03-24', '2024-03-26', 3, NULL, 'admin'),
	(4, 'admin', 'งานจัดเลี้ยง', '0852222222', 'Test 17', '2024-03-26', '2024-03-27', 2, NULL, 'admin'),
	(5, 'admin', 'งานจัดเลี้ยงโต้รุ่ง', '0852222333', 'Test 19', '2024-03-21', '2024-03-23', 3, NULL, 'admin'),
	(6, 'admin', 'งานจัดเลี้ยงโต้รุ่ง2', '0852274333', 'Test 20', '2024-03-28', '2024-03-31', 4, NULL, 'admin'),
	(7, 'admin', 'งานจัดเลี้ยงโต้รุ่ง2', '0852274333', 'Test 21', '2024-03-26', '2024-03-27', 2, NULL, 'admin'),
	(8, 'admin', 'งานจัดเลี้ยงโต้รุ่ง99', '0852274333', 'XXX', '2024-03-29', '2024-03-31', 3, NULL, 'admin'),
	(9, 'สิทธิศักดิ์ ชูเล็ก', 'นำเสนองาน', '05554665122', 'เพื่อคะแนนสูงๆ เกรด A ในวิชา..........\n946-445 Web-based Business Application Development\n948-441 Advanced Database System และ\n948-447 Software Testing and Quality Assurance', '2024-03-25', '2024-03-25', 1, NULL, '6450110013'),
	(10, 'สิทธิศักดิ์ ชูเล็ก', 'งานจัดเลี้ยงโต้รุ่ง2', '0852274333', 'ทดสอบการยืม', '2024-03-27', '2024-03-28', 2, NULL, '6450110013'),
	(11, 'admin', 'งานจัดเลี้ยงโต้รุ่ง2', '0852274333', 'ทดสอบ', '2024-04-04', '2024-04-06', 3, NULL, 'admin');

-- Dumping structure for table borrowing_system.borrowing_detail
CREATE TABLE IF NOT EXISTS `borrowing_detail` (
  `borrowId` int(11) DEFAULT NULL,
  `equipmentId` int(11) DEFAULT NULL,
  `borrowQty` int(11) DEFAULT NULL,
  `approveStatus` varchar(10) DEFAULT 'reserve',
  `returnStatus` varchar(5) DEFAULT 'waite',
  `damageQty` int(11) DEFAULT 0,
  `notation` text DEFAULT '-',
  KEY `FK_borrowing_detail_equipment_list` (`equipmentId`),
  KEY `FK_borrowing_detail_borrowing` (`borrowId`),
  CONSTRAINT `FK_borrowing_detail_borrowing` FOREIGN KEY (`borrowId`) REFERENCES `borrowing` (`borrowId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_borrowing_detail_equipment_list` FOREIGN KEY (`equipmentId`) REFERENCES `equipment_list` (`equipmentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table borrowing_system.borrowing_detail: ~11 rows (approximately)
INSERT IGNORE INTO `borrowing_detail` (`borrowId`, `equipmentId`, `borrowQty`, `approveStatus`, `returnStatus`, `damageQty`, `notation`) VALUES
	(2, 1, 5, 'yes', 'yes', 0, '-'),
	(3, 1, 9, 'yes', 'no', 0, '-'),
	(3, 2, 4, 'reserve', 'waite', 0, '-'),
	(4, 3, 5, 'no', 'null', 0, '-'),
	(5, 2, 5, 'yes', 'no', 0, '-'),
	(6, 5, 5, 'reserve', 'waite', 0, '-'),
	(6, 6, 7, 'no', 'null', 0, '-'),
	(7, 2, 3, 'no', 'null', 0, '-'),
	(8, 1, 5, 'yes', 'yes', 0, '-'),
	(9, 1, 5, 'yes', 'yes', 0, '-'),
	(10, 3, 2, 'no', 'null', 0, '-'),
	(11, 2, 3, 'reserve', 'waite', 0, '-'),
	(11, 5, 7, 'reserve', 'waite', 0, '-');

-- Dumping structure for table borrowing_system.category
CREATE TABLE IF NOT EXISTS `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(30) NOT NULL DEFAULT '',
  `measure` varchar(15) DEFAULT NULL,
  `categoryImg` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table borrowing_system.category: ~13 rows (approximately)
INSERT IGNORE INTO `category` (`categoryId`, `categoryName`, `measure`, `categoryImg`) VALUES
	(1, 'ผ้า', 'ผืน', '931ab111-193c-41f7-8558-327ce688c322.jpg'),
	(2, 'แก้วน้ำ', 'ใบ', 'e7a96995-329f-4058-bb19-265b6eecef36.jpg'),
	(3, 'เสื้อ', 'ตัว', '23e7351c-d3a5-473a-a1e3-d6c82ecbef0c.jpg'),
	(4, 'จาน', 'ใบ', '81304596-2b57-4d30-8a9a-ef77fcea005b.jpg'),
	(7, 'กะละมัง', 'ใบ', '57c798d3-041c-4ba0-8c0c-ff4048836150.jpg'),
	(10, 'ช้อน-ซ่อม', 'คู่', '5dfef965-7c8f-440c-a21b-5052b23159cb.jpg'),
	(11, 'กระดาษทิชชู่', 'ม้วน', 'e6ec9286-7b9d-4b2e-988d-ea58f73a7731.jpg'),
	(12, 'หมวกฟาง', 'ใบ', '22ae5fcf-57aa-4da6-b684-bba1d8ba2073.jpg'),
	(13, 'สมุด', 'เล่ม', '3feaeabb-0a3e-4999-a991-0b6b3469bf18.jpg'),
	(15, 'ช้อน', 'คู่', '2f034d83-0a7b-44a5-819d-70537dfcd9f3.jpg'),
	(16, 'กระดาษทิชชู่', 'ม้วน', '0128b2aa-9e0f-4333-993d-30319ec42f53.jpg'),
	(25, 'ผ้าปูโต๊ะ', 'ผืน', '5c426a20-1506-419f-ad90-3e57a75347b6.jpg');

-- Dumping structure for table borrowing_system.equipment_list
CREATE TABLE IF NOT EXISTS `equipment_list` (
  `equipmentId` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentName` varchar(30) DEFAULT NULL,
  `equipmentImg` varchar(255) DEFAULT NULL,
  `invenQty` int(11) DEFAULT 0,
  `permission` int(1) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`equipmentId`) USING BTREE,
  KEY `FK_equipment_list_category` (`categoryId`),
  CONSTRAINT `FK_equipment_list_category` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table borrowing_system.equipment_list: ~9 rows (approximately)
INSERT IGNORE INTO `equipment_list` (`equipmentId`, `equipmentName`, `equipmentImg`, `invenQty`, `permission`, `categoryId`) VALUES
	(1, 'ผ้าขาว', '3eb19059-170c-4154-9888-af28218e047d.jpg', 50, 1, 1),
	(2, 'จานกระเบื้อง', '79cf58ae-25f7-4521-8ef4-cf6e5722fb12.jpg', 127, 2, 4),
	(3, 'ช้อนเงิน', '28d86688-306e-4a20-b3ce-3ed9d27cb4ed.jpg', 10, 1, 10),
	(4, 'สมุดจดบันทึก', 'a80775e6-4ca5-4c45-b15f-208649f78c58.jpg', 0, 3, 13),
	(5, 'กระดาษทิชชู่', 'b7b97ec0-b7ec-4101-b397-ffc159f1f50d.jpg', 70, 2, 11),
	(6, 'ผ้าปูโต๊ะสีเหลือง', 'a3e99158-2283-42b6-b1db-6a70b6e374ea.jpg', 82, 3, 1),
	(7, 'สมุดไดอารี่', 'dbfe9083-69cf-4974-81cd-83c5bff74bd9.jpg', 56, 3, 13),
	(9, 'เสื้อกีฬา', 'd41a0f75-8319-4cf3-8d52-95b9d631af92.jpg', 23, 1, 3),
	(12, 'สมุดโน๊ต', '0dd2cc12-6a1b-4102-84be-7aeb78951f1c.jpg', 52, 1, 13),
	(26, 'ช้อนงานแต่ง', 'b2b43d25-0503-47a7-8578-74490555a51f.jpg', 68, 2, 15),
	(28, 'ผ้าปูโต๊ะสีรุ้ง', 'f8ace0e7-19ff-4d11-8cdb-b15c9209dabf.jpg', 82, 1, 25);

-- Dumping structure for table borrowing_system.selected_reserve
CREATE TABLE IF NOT EXISTS `selected_reserve` (
  `series` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentId` int(11) DEFAULT NULL,
  `reserveQty` int(11) DEFAULT NULL,
  `userId` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`series`),
  KEY `FK_selected_reserve_equipment_list` (`equipmentId`) USING BTREE,
  CONSTRAINT `FK_selected_reserve_equipment_list` FOREIGN KEY (`equipmentId`) REFERENCES `equipment_list` (`equipmentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table borrowing_system.selected_reserve: ~73 rows (approximately)
INSERT IGNORE INTO `selected_reserve` (`series`, `equipmentId`, `reserveQty`, `userId`) VALUES
	(5, 1, 3, 'admin'),
	(6, 1, 3, 'admin'),
	(7, 5, 6, 'admin'),
	(8, 5, 1, 'admin'),
	(9, 6, 3, 'admin'),
	(10, 3, 2, 'admin'),
	(11, 2, 5, 'admin'),
	(12, 6, 3, 'admin'),
	(13, 2, 6, 'admin'),
	(14, 2, 2, 'admin'),
	(15, 2, 2, 'admin'),
	(16, 3, 3, 'admin'),
	(17, 2, 2, 'admin'),
	(18, 6, 6, 'admin'),
	(19, 6, 6, 'admin'),
	(20, 6, 6, 'admin'),
	(21, 6, 6, 'admin'),
	(22, 6, 6, 'admin'),
	(23, 6, 6, 'admin'),
	(24, 6, 6, 'admin'),
	(25, 6, 6, 'admin'),
	(26, 6, 6, 'admin'),
	(27, 6, 6, 'admin'),
	(28, 6, 6, 'admin'),
	(29, 6, 6, 'admin'),
	(30, 2, 2, 'admin'),
	(31, 2, 6, 'admin'),
	(32, 2, 5, 'admin'),
	(38, 2, 5, 'admin'),
	(39, 2, 5, 'admin'),
	(40, 3, 2, 'admin'),
	(41, 5, 1, 'admin'),
	(42, 6, 3, 'admin'),
	(44, 1, 3, 'admin'),
	(45, 2, 2, 'admin'),
	(46, 2, 3, 'admin'),
	(47, 3, 2, 'admin'),
	(49, 2, 2, 'admin'),
	(50, 1, 2, 'admin'),
	(51, 5, 3, 'admin'),
	(52, 1, 5, 'admin'),
	(53, 2, 3, 'admin'),
	(54, 1, 3, 'admin'),
	(55, 2, 5, 'admin'),
	(56, 1, 2, 'admin'),
	(57, 2, 6, 'admin'),
	(58, 3, 5, 'admin'),
	(59, 1, 2, 'admin'),
	(62, 1, 4, 'admin'),
	(63, 3, 4, 'admin'),
	(64, 1, 5, 'admin'),
	(65, 3, 3, 'admin'),
	(66, 1, 3, 'admin'),
	(67, 3, 5, 'admin'),
	(68, 1, 3, 'admin'),
	(69, 3, 2, 'admin'),
	(70, 2, 9, 'admin'),
	(71, 3, 5, 'admin'),
	(72, 1, 5, 'admin'),
	(73, 1, 5, 'admin'),
	(74, 1, 9, 'admin'),
	(75, 2, 4, 'admin'),
	(76, 3, 5, 'admin'),
	(77, 2, 6, 'admin'),
	(78, 2, 5, 'admin'),
	(79, 5, 5, 'admin'),
	(80, 6, 7, 'admin'),
	(81, 2, 3, 'admin'),
	(87, 1, 5, 'admin'),
	(88, 1, 5, '6450110013'),
	(90, 3, 2, '6450110013'),
	(91, 6, 3, '6450110013'),
	(92, 6, 5, '6450110013'),
	(93, 2, 3, 'admin'),
	(94, 5, 7, 'admin');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
