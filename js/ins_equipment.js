// Show category list
$(document).ready(function () {
  var dataLoaded = false;
  $("#categoryId").click(function () {
    if (!dataLoaded) {
      $.ajax({
        type: "POST",
        url: "./api/category/fetch_category.php",
        dataType: "json",
        success: function (res) {
          console.log(res.data);
          // $("#categoryId").empty();
          var categoryData = res.data;
          for (var i = 0; i < categoryData.length; i++) {
            $("#categoryId").append(
              '<option value="' +
                categoryData[i].categoryId +
                '">' +
                categoryData[i].categoryName +
                "</option>"
            );
          }
        },
      });
    }
    dataLoaded = true;
  });
});

$(document).ready(function () {
  // C: Create
  $("#add_equipment_btn").click(function () {
    $("#form_equipment")[0].reset();
    $(".modal-title").text("เพิ่มรายการอุปกรณ์");
    $("#action").val("เพิ่มข้อมูล");
    $("#operation").val("insert");
  });

  $(document).on("submit", "#form_equipment", function (event) {
    event.preventDefault();
    var formContent = new FormData(this);
    var equipmentId = $("#equipmentId").val();
    $("#equipmentId").attr("readonly", false);
    var equipmentName = $("#equipmentName").val();
    var invenQty = $("#invenQty").val();
    var permission = $("#permission").val();
    var categoryId = $("#categoryId").val();
    var equipmentImg = $("#equipmentImg").val();

    console.log(formContent);

    if (
      equipmentId != "" &&
      equipmentName != "" &&
      invenQty != "" &&
      permission != "" &&
      categoryId != "" &&
      equipmentImg != ""
    ) {
      // Check for duplicate primary key
      $.ajax({
        url: "./api/equipment/fetch_equipment.php",
        type: "POST",
        dataType: "json",
        success: function (res) {
          var equipmentData = res.data;
          var foundDuplicatePK = false;

          for (var i = 0; i < equipmentData.length; i++) {
            if (equipmentId == equipmentData[i].equipmentId) {
              foundDuplicatePK = true;
              break;
            }
          }

          if (foundDuplicatePK) {
            Swal.fire({
              icon: "error",
              title: "เพิ่มข้อมูลล้มเหลว",
              text:
                'รหัสนี้ถูกใช้สำหรับ "' +
                equipmentData[i].equipmentName +
                '" แล้ว',
            });
          } else {
            // Insert new data if primary key is not a duplicate
            $.ajax({
              url: "./api/equipment/manage_equipment.php",
              method: "POST",
              data: formContent,
              contentType: false,
              processData: false,
              success: function (data) {
                console.log(data);
                $("#form_equipment")[0].reset();
                $("#modal-equipment .btn-close").click();
                Swal.fire({
                  icon: "success",
                  title: "ยืนยันการเพิ่มข้อมูล",
                  text: 'เพิ่ม "' + equipmentName + '" สำเร็จ',
                });
                dataTable.ajax.reload();
              },
              error: function (xhr, status, error) {
                console.log("Error occurred during AJAX request:", error);
                Swal.fire({
                  icon: "error",
                  title: "เพิ่มข้อมูลล้มเหลว",
                });
              },
            });
          }
        },
        error: function (xhr, status, error) {
          console.log("Error occurred during AJAX request:", error);
          Swal.fire({
            icon: "error",
            title: "เพิ่มข้อมูลล้มเหลว",
          });
        },
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "เพิ่มข้อมูลล้มเหลว",
        text: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
      });
    }
  });

  // R: Read
  var dataTable = $("#equipmentList").DataTable({
    processing: true,
    ajax: {
      url: "./api/equipment/fetch_equipment.php",
      type: "POST",
    },
    scrollCollapse: true,
    scrollX: true,
    scrollY: "50vh",
    columns: [
      { data: "equipmentId" },
      {
        data: "equipmentImg",
        render: function (data, type, row) {
          return (
            '<img src="./upload_files/equipment/' + data + '" height="100">'
          );
        },
      },
      { data: "equipmentName" },
      { data: "invenQty" },
      {
        data: "permission",
        render: function (data, type, row) {
          switch (data) {
            case 1:
              return "นักศึกษา";
            case 2:
              return "อาจารย์/บุคลากร";
            case 3:
              return "เจ้าหน้าที่";
            default:
              return "เกิดข้อผิดพลาด";
          }
        },
      },
      { data: "categoryName" },
      { data: "measure" },
      {
        data: "update",
        render: function (data, type, row) {
          return (
            '<button type="button" name="update" id="update-equip-' +
            data +
            '" class="btn btn-primary btn update">แก้ไข</button></button>'
          );
        },
      },
      {
        data: "delete",
        render: function (data, type, row) {
          return (
            '<button type="button" name="delete" id="delete-equip-' +
            data +
            '" class="btn btn-danger btn delete">ลบ</button>'
          );
        },
      },
    ],
    columnDefs: [{ orderable: false, targets: [1, 7, 8] }],
  });

  // U: Update
  $(document).on("click", ".update", function () {
    var m = $(this).attr("id");
    var manageId = m.toString();
    console.log("Manage ID:", manageId);
    $.ajax({
      url: "./api/equipment/single_equipment.php",
      method: "POST",
      data: { manageId: manageId },
      dataType: "json",
      success: function (data) {
        $("#modal-equipment").modal("show");
        $(".modal-title").text("แก้ไขรายการอุปกรณ์");
        $("#equipmentId").val(data.equipmentId);
        $("#equipmentId").attr("readonly", true);
        $("#equipmentName").val(data.equipmentName);
        $("#invenQty").val(data.invenQty);
        $("#permission").val(data.permission);
        $("#categoryId").val(data.categoryId);
        $("#equipmentImgLabel").text(data.equipmentImg);
        $("#uploadedEquipmentImg").html(
          '<img src="./upload_files/equipment/' +
            data.equipmentImg +
            '" height="150" class="rounded-2">'
        );
        $("#action").val("แก้ไข");
        $("#operation").val("update");

        $(document).on("submit", "#form_equipment", function (event) {
          event.preventDefault();
          var equipmentId = $("#equipmentId").val();
          $("#equipmentId").attr("readonly", true);
          var equipmentName = $("#equipmentName").val();
          var invenQty = $("#invenQty").val();
          var permission = $("#permission").val();
          var categoryId = $("#categoryId").val();

          console.log(new FormData(this));

          if (
            equipmentId != "" &&
            equipmentName != "" &&
            invenQty != "" &&
            permission != "" &&
            categoryId != ""
          ) {
            $.ajax({
              url: "./api/equipment/manage_equipment.php",
              method: "POST",
              data: new FormData(this),
              contentType: false,
              processData: false,
              success: function (data) {
                console.log(data);
                $("#form_equipment")[0].reset();
                $("#modal-equipment .btn-close").click();
                Swal.fire({
                  icon: "success",
                  title: "ยืนยันการแก้ไขข้อมูล",
                  text: 'แก้ไข "' + equipmentName + '" สำเร็จ',
                });
                dataTable.ajax.reload();
              },
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "แก้ไขข้อมูลล้มเหลว",
              text: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
            });
          }
        });
      },
    });
  });

  // D: Delete
  $(document).on("click", ".delete", function () {
    var m = $(this).attr("id");
    var manageId = m.toString();
    console.log("Manage ID:", manageId);
    $.ajax({
      url: "./api/equipment/single_equipment.php",
      method: "POST",
      data: { manageId: manageId },
      dataType: "json",
      success: function (data) {
        Swal.fire({
          title: "ยืนยันการลบข้อมูล",
          text: 'ต้องการลบ "' + data.equipmentName + '" หรือไม่?',
          icon: "warning",
          showCancelButton: true,
          cancelButtonColor: "#d33",
          cancelButtonText: "ยกเลิก",
          confirmButtonColor: "#3085d6",
          confirmButtonText: "ยืนยัน",
        }).then(function (result) {
          var showName = data.equipmentName;
          if (result.isConfirmed) {
            $.ajax({
              url: "./api/equipment/manage_equipment.php",
              method: "POST",
              data: { manageId: manageId, operation: "delete" },
              success: function (data) {
                Swal.fire({
                  title: "ยืนยันการลบข้อมูล",
                  text: 'ลบ "' + showName + '" สำเร็จ',
                  icon: "success",
                });
                dataTable.ajax.reload();
              },
            });
          }
        });
      },
    });
  });
});
