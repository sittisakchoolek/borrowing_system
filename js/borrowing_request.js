// Show equipment list
$(document).ready(function () {
  // User authebtication
  $.ajax({
    type: "POST",
    url: "./api/asset/userAuthData.php",
    dataType: "json",
    success: function (res) {
      console.log(res.userId);
      console.log(res.userName);
      console.log(res.userStatus);
      $("#userId").val(res.userId);
      $("#name").val(res.userName);
      var userId = res.userId;
      var userName = res.userName;
      var userStatus = res.userStatus;

      switch (userStatus) {
        case 1:
          $(".modal-title").text("รายการอุปกรณ์สำหรับการยืม (นักศึกษา)");
          break;
        case 2:
          $(".modal-title").text("รายการอุปกรณ์สำหรับการยืม (อาจารย์/บุคลากร)");
          break;
        case 3:
          $(".modal-title").text("รายการอุปกรณ์สำหรับการยืม (เจ้าหน้าที่)");
          break;
      }

      // Show equiptment list depend on user permission
      $.ajax({
        type: "POST",
        url: "./api/equipment/fetch_equipment.php",
        data: { userStatus: userStatus },
        dataType: "json",
        success: function (res) {
          console.log(res.data);
          // $("#equipmentId").empty();
          var equipmentData = res.data;
          for (var i = 0; i < equipmentData.length; i++) {
            var html = "";
            var equipmentId = equipmentData[i].equipmentId;
            var equipmentImg = equipmentData[i].equipmentImg;
            var equipmentName = equipmentData[i].equipmentName;
            var categoryName = equipmentData[i].categoryName;
            var invenQty = equipmentData[i].invenQty;
            var measure = equipmentData[i].measure;
            var count = 0;
            var trReserveList;

            html += `
                <div class="container-fluid bg-light-subtle shadow-sm mt-3 mb-3 rounded-2">
                    <div class="row">
                        <div class="col-xl-3 col-lg-5 text-center mt-2 mb-2">
                            <img src="./upload_files/equipment/${equipmentImg}" height="150px"
                                class="rounded-2" alt="${equipmentName}">
                        </div>
                        <div class="col-xl-5 col-lg-7 mt-2 mb-2">
                            <h3>${equipmentName}</h3>
                            <hr class="m-0 mb-3">
                            <div class="row mb-3">
                                <div class="col-4 bg-info-subtle rounded-2">หมวดหมู่</div>
                                <div class="col-1"></div>
                                <div class="col-7 fs-5">${categoryName}</div>
                            </div>
                `;
            if (invenQty > 0) {
              html += `
                        <div class="row">
                          <div class="col-4 bg-info-subtle rounded-2">สามารถยืมได้</div>
                          <div class="col-1"></div>
                          <div class="col-7 fs-5">${invenQty}</div>
                        </div>
                      `;
            } else {
              html += `
                        <div class="row">
                          <div class="col-4 bg-danger-subtle rounded-2">ไม่สามารถยืมได้</div>
                          <div class="col-1"></div>
                          <div class="col-7 text-danger fs-5">${invenQty}</div>
                        </div>
                      `;
            }
            html += `
                        </div>
                        <div class="col-xl-4 col-lg-12 mt-2 mb-2 row align-items-center">

                            <form id="form_reserve_request_${equipmentId}">
                                <input type="hidden" name="eReserveId" id="eReserveId" value="${equipmentId}">
                                <input type="hidden" name="eReserveImg" id="eReserveImg" value="${equipmentImg}">
                                <input type="hidden" name="eReserveName" id="eReserveName" value="${equipmentName}">
                                <input type="hidden" name="cReserveName" id="cReserveName" value="${categoryName}">
                                <input type="hidden" name="eInvenQty" id="eInvenQty" value="${invenQty}">
                                <input type="hidden" name="cMeasure" id="cMeasure" value="${measure}">
                                <div class="input-group pt-2 pb-2">
                                    <label class="input-group-text" for="reserveQty-${equipmentId}">ต้องการยืม</label>
                                    <input type="number" name="reserveQty-${equipmentId}" id="reserveQty-${equipmentId}" min="0" class="form-control">
                                    <span class="input-group-text">${measure}</span>
                                </div>
                                <div class="d-flex pt-2 pb-2">
                                    <input type="submit" id="reserve-equip-id-${equipmentId}" value="จอง${equipmentName}" class="btn btn-primary col">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
              `;
            $("#showEquipmentList").append(html);

            // Update inventory quatity and display reserve detail
            $(document).on(
              "submit",
              "#form_reserve_request_" + equipmentId,
              function (event) {
                event.preventDefault();
                var eReserveId = $(this).find("#eReserveId").val();
                var equipmentName = $(this).find("#eReserveName").val();
                var invenQty = $(this).find("#eInvenQty").val();
                var reserveQty = $(this)
                  .find("#reserveQty-" + eReserveId)
                  .val();

                console.log(eReserveId);
                console.log(equipmentName);
                console.log(invenQty);
                console.log(reserveQty);

                if (
                  eReserveId != "" &&
                  reserveQty != "" &&
                  userId != "" &&
                  invenQty != ""
                ) {
                  if (parseInt(reserveQty) > parseInt(invenQty)) {
                    Swal.fire({
                      icon: "error",
                      title: "การจองล้มเหลว",
                      text:
                        'รายการ "' +
                        equipmentName +
                        '" จองได้ ' +
                        invenQty +
                        " " +
                        measure,
                    });
                  } else {
                    $.ajax({
                      url: "./api/borrow_return/selected_reserve.php",
                      type: "POST",
                      dataType: "json",
                      data: {
                        equipmentId: eReserveId,
                        reserveQty: reserveQty,
                        userId: userId,
                      },
                      success: function (data) {
                        console.log(data);
                        $("#equipmentListModal .btn-close").click();
                        Swal.fire({
                          icon: "success",
                          title: "ยืนยันการจอง",
                          text:
                            'จอง "' +
                            data.equipmentName +
                            '" จำนวน ' +
                            data.reserveQty +
                            " " +
                            data.measure +
                            " สำเร็จ",
                        });

                        var html = "";
                        var equipmentId = data.equipmentId;
                        var equipmentImg = data.equipmentImg;
                        var equipmentName = data.equipmentName;
                        var reserveQty = data.reserveQty;
                        var measure = data.measure;
                        var cancel = data.cancel;
                        trReserveList = data.cancel;
                        count++;

                        html += `
                                <tr id="tr-${cancel}" align="center">
                                    <td>${count}</td>
                                    <td><img src="./upload_files/equipment/${equipmentImg}" alt="" class="rounded-2" width="230px"></td>
                                    <td>${equipmentName}</td>
                                    <td>${reserveQty}</td>
                                    <td>${measure}</td>
                                    <td><button type="button" name="cancel" id="cancel-reserve-${cancel}" class="btn btn-danger">ยกเลิกการจอง</button></td>
                                </tr>
                                `;

                        $("#showReserveList").append(html);

                        $("#showReserveList").on(
                          "click",
                          "[id^='cancel-reserve-']",
                          function () {
                            var cancel = $(this).attr("id").split("-")[2]; // Extract the cancel ID from the button's ID
                            var equipmentName = $(this)
                              .closest("tr")
                              .find("td:eq(2)")
                              .text(); // Assuming equipmentName is in the second <td>
                            var reserveQty = $(this)
                              .closest("tr")
                              .find("td:eq(3)")
                              .text(); // Assuming reserveQty is in the third <td>
                            var measure = $(this)
                              .closest("tr")
                              .find("td:eq(4)")
                              .text(); // Assuming measure is in the fourth <td>

                            Swal.fire({
                              title: "ยกเลิกการจอง",
                              text:
                                'ต้องการยกเลิกการจอง "' +
                                equipmentName +
                                '" จำนวน ' +
                                reserveQty +
                                " " +
                                measure +
                                " หรือไม่?",
                              icon: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#3085d6",
                              cancelButtonColor: "#d33",
                              confirmButtonText: "ยืนยัน",
                              cancelButtonText: "ยกเลิก",
                            }).then((result) => {
                              if (result.isConfirmed) {
                                $.ajax({
                                  url: "./api/borrow_return/selected_reserve.php",
                                  type: "POST",
                                  data: {
                                    equipmentId: equipmentId,
                                    cancel: cancel,
                                    reserveQty: reserveQty,
                                  },
                                  success: function (data) {
                                    console.log(data);
                                    Swal.fire({
                                      title: "ยืนยันการยกเลิก",
                                      text:
                                        'ยกเลิกการจอง "' +
                                        equipmentName +
                                        '" สำเร็จ',
                                      icon: "success",
                                    });

                                    $("#showReserveList #tr-" + cancel).remove();
                                  },
                                  error: function (xhr, status, error) {
                                    console.log("Error occurred:", error);
                                  },
                                });
                              }
                            });
                          }
                        );

                      },
                      error: function (xhr, status, error) {
                        console.log("Error occurred:", error);
                      },
                    });
                  }
                }
              }
            );
          }
        },
        error: function (xhr, status, error) {
          console.log("Error occurred:", error);
        },
      });
    },
    error: function (xhr, status, error) {
      console.log("Error occurred:", error);
    },
  });

  $(document).on("submit", "#form_brr_request", function () {
    //  Insert to borrowing and borrowing_detail relation
    event.preventDefault();
    var userId = $("#userId").val();
    var name = $("#name").val();
    var title = $("#title").val();
    var telNumber = $("#telNumber").val();
    var borrowDate = $("#borrowDate").val();
    var returnDate = $("#returnDate").val();
    var objective = $("#objective").val();
    var fileUploadPdf = $("#fileUploadPdf").val();
    var reserveSeries = [];

    // $("#showReserveList #tr-" + trReserveList)
    $("tr[id^='tr-']").each(function() {
      var currentSeries = $(this).attr("id").split("-")[1];
      reserveSeries.push(currentSeries);
    });
    
    console.log("userId:        ", userId);
    console.log("name:          ", name);
    console.log("title:         ", title);
    console.log("telNumber:     ", telNumber);
    console.log("borrowDate:    ", borrowDate);
    console.log("returnDate:    ", returnDate);
    console.log("objective:     ", objective);
    console.log("fileUploadPdf: ", fileUploadPdf);
    console.log("reserveSeries: ", reserveSeries);

    $.ajax({
      url: "./api/borrow_return/manage_borrowing.php",
      type: "POST",
      data: {
        userId: userId,
        name: name,
        title: title,
        telNumber: telNumber,
        borrowDate: borrowDate,
        returnDate: returnDate,
        objective: objective,
        fileUploadPdf: fileUploadPdf,
        reserveSeries: reserveSeries,
        operation: "insert"
      },
      // contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      success: function (data) {
        console.log(data);
        Swal.fire({
          icon: "success",
          title: "ส่งคำร้องสำเร็จ",
          text: "กรุณารอผลการอนุมัติจากเจ้าหน้าที่",
        });
      },
      error: function (xhr, status, error) {
        console.log("Error occurred:", error);
      },
    });           
  });

});