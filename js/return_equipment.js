var dataTable;

$(document).ready(function () {
    // R: Read
    dataTable = $("#returnList").DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        url: "./api/borrow_return/fetch_return.php",
        type: "POST",
        // data: function (data) {
        //   // Add form data to the request
        //   data.borrowStatus = $("#borrowStatus").val();
        //   data.startDate = $("#startDate").val();
        //   data.endDate = $("#endDate").val();
        // },
      },
      scrollCollapse: true,
      scrollX: true,
      scrollY: "50vh",
      columns: [
        { data: "borrowId" },
        { data: "equipmentName" },
        { data: "name" },
        { data: "borrowQty" },
        { data: "borrowDate" },
        { data: "returnDate" },
        { 
          data: "returnStatus",
          render: function (data, type, row) {
            if (data == "yes") {
              return `<span class="text-bg-success  p-2 rounded-2 w-100">คืนแล้ว</span>`;
            } else if (data == "no") {
              return `<span class="text-bg-warning  p-2 rounded-2 w-100">ค้างคืน</span>`;
            }
          }
        },
        {
          data: "return",
          render: function (data, type, row) {
            return (
              `<button type="button" id="` +
              data +
              `" class="btn btn-primary">คืนรายการ</button>`
            );
          },
        },
      ],
      columnDefs: [{ orderable: false, targets: [6] }],
    });
  
    $(document).on("click", 'button[id^="return-"]', function () {
      var buttonId = $(this).attr("id");
      var parts = buttonId.split("-");
  
      var borrowId = parts[1];
      var equipmentId = parts[2];
  
      console.log(borrowId);
      console.log(equipmentId);
  
      $.ajax({
        url: "./api/borrow_return/manage_borrowing.php",
        method: "POST",
        data: {
          borrowId: borrowId,
          equipmentId: equipmentId,
          operation: "return",
        },
        success: function () {
          Swal.fire({
            title: "ยืนยันการส่งคืนรายการ",
            icon: "success"
          });
          dataTable.ajax.reload();
        },
      });
    });
  
    $("#filter").on("click", function () {
      dataTable.ajax.reload();
    });
  
  });