var dataTable;

$(document).ready(function () {
  // R: Read
  dataTable = $("#borrowRequestList").DataTable({
    processing: true,
    serverSide: true,
    ajax: {
      url: "./api/borrow_return/fetch_consider.php",
      type: "POST",
      // data: function (data) {
      //   // Add form data to the request
      //   data.borrowStatus = $("#borrowStatus").val();
      //   data.startDate = $("#startDate").val();
      //   data.endDate = $("#endDate").val();
      // },
    },
    scrollCollapse: true,
    scrollX: true,
    scrollY: "50vh",
    columns: [
      { data: "borrowId" },
      { data: "equipmentName" },
      { data: "name" },
      { data: "borrowQty" },
      { data: "borrowDate" },
      { data: "returnDate" },
      {
        data: "detail",
        render: function (data, type, row) {
          return (
            `<button type="button" id="` +
            data +
            `" class="btn btn-outline-secondary"
            data-bs-toggle="modal" data-bs-target="#modal-detail">
            รายระเอียด</button>`
          );
        },
      },
      {
        data: "approveStatus",
        render: function (data, type, row) {
          if (data == "reserve") {
            return `<span class="text-bg-warning p-2 rounded-2 w-100">จอง</span>`;
          } else if (data == "yes") {
            return `<span class="text-bg-success  p-2 rounded-2 w-100">อนุมัติ</span>`;
          } else if (data == "no") {
            return `<span class="text-bg-secondary  p-2 rounded-2 w-100">ไม่อนุมัติ</span>`;
          }
        },
      },
      {
        data: "approve",
        render: function (data, type, row) {
          return (
            `<button type="button" id="` +
            data +
            `" class="btn btn-primary">อนุมัติ</button>`
          );
        },
      },
      {
        data: "notApprove",
        render: function (data, type, row) {
          return (
            `<button type="button" id="` +
            data +
            `" class="btn btn-danger">ไม่อนุมัติ</button>`
          );
        },
      },
    ],
    columnDefs: [{ orderable: false, targets: [6, 7, 8, 9] }],
  });

  $(document).on("click", 'button[id^="approve-"]', function () {
    var buttonId = $(this).attr("id");
    var parts = buttonId.split("-");

    var borrowId = parts[1];
    var equipmentId = parts[2];

    console.log(borrowId);
    console.log(equipmentId);

    $.ajax({
      url: "./api/borrow_return/manage_borrowing.php",
      method: "POST",
      data: {
        borrowId: borrowId,
        equipmentId: equipmentId,
        operation: "approve",
      },
      success: function () {
        Swal.fire({
          title: "อนุมัติรายการสำเร็จ",
          icon: "success",
        });
        dataTable.ajax.reload();
      },
    });
  });

  $(document).on("click", 'button[id^="detail-"]', function () {
    var buttonId = $(this).attr("id");
    var parts = buttonId.split("-");

    var borrowId = parts[1];
    var equipmentId = parts[2];

    console.log(borrowId);
    console.log(equipmentId);

    $.ajax({
      url: "./api/borrow_return/fetch_consider.php",
      method: "POST",
      dataType: "json",
      success: function (res) {
        console.log(res);
        var detailData = res.data;
        for (var i = 0; i < detailData.length; i++) {
          if (
            borrowId == detailData[i].borrowId &&
            equipmentId == detailData[i].equipmentId
          ) {
            var html = "";
            var title = detailData[i].title;
            var name = detailData[i].name;
            var telNumber = detailData[i].telNumber;
            var objective = detailData[i].objective;

            console.log(title);
            console.log(name);
            console.log(telNumber);
            console.log(objective);

            html += `
              <h1>${title}</h1></h1>
              <p>ชื่อ: ${name}</p>
              <p>เบอร์ติดต่อ: ${telNumber}</p>
              <p>วัตถุประสงค์การยืม: ${objective}</p>
            `;

            $("#showBorrowDetail").html(html);
          }
        }
      },
    });
  });

  $(document).on("click", 'button[id^="notApprove-"]', function () {
    var buttonId = $(this).attr("id");
    var parts = buttonId.split("-");

    var borrowId = parts[1];
    var equipmentId = parts[2];

    console.log(borrowId);
    console.log(equipmentId);

    $.ajax({
      url: "./api/borrow_return/manage_borrowing.php",
      method: "POST",
      data: {
        borrowId: borrowId,
        equipmentId: equipmentId,
        operation: "notApprove",
      },
      success: function () {
        Swal.fire({
          title: "ปฐิเสธรายการสำเร็จ",
          icon: "success",
        });
        dataTable.ajax.reload();
      },
    });
  });

  $("#filter").on("click", function () {
    dataTable.ajax.reload();
  });
});
