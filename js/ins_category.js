$(document).ready(function () {
  // C: Create
  $("#add_category_btn").click(function () {
    $("#form_category")[0].reset();
    $(".modal-title").text("เพิ่มหมวดหมู่อุปกรณ์");
    $("#action").val("เพิ่มข้อมูล");
    $("#operation").val("insert");
  });

  $(document).on("submit", "#form_category", function (event) {
    event.preventDefault();
    var formContent = new FormData(this);
    var categoryId = $("#categoryId").val();
    $("#categoryId").attr("readonly", false);
    var categoryName = $("#categoryName").val();
    var measure = $("#measure").val();
    var categoryImg = $("#categoryImg").val();

    if (
      categoryId != "" &&
      categoryName != "" &&
      measure != "" &&
      categoryImg != ""
    ) {
      // Check for duplicate primary key
      $.ajax({
        url: "./api/category/fetch_category.php",
        type: "POST",
        dataType: "json",
        success: function (res) {
          var categoryData = res.data;
          var foundDuplicatePK = false;

          for (var i = 0; i < categoryData.length; i++) {
            if (categoryId == categoryData[i].categoryId) {
              foundDuplicatePK = true;
              break;
            }
          }

          if (foundDuplicatePK) {
            Swal.fire({
              icon: "error",
              title: "เพิ่มข้อมูลล้มเหลว",
              text:
                'รหัสนี้ถูกใช้สำหรับ "' +
                categoryData[i].categoryName +
                '" แล้ว',
            });
          } else {
            // Insert new data if primary key is not a duplicate
            $.ajax({
              url: "./api/category/manage_category.php",
              method: "POST",
              data: formContent,
              contentType: false,
              processData: false,
              success: function (data) {
                console.log(data);
                $("#form_category")[0].reset();
                $("#modal-category .btn-close").click();
                Swal.fire({
                  icon: "success",
                  title: "ยืนยันการเพิ่มข้อมูล",
                  text: 'เพิ่ม "' + categoryName + '" สำเร็จ',
                });
                dataTable.ajax.reload();
              },
              error: function (xhr, status, error) {
                console.log("Error occurred during AJAX request:", error);
                Swal.fire({
                  icon: "error",
                  title: "เพิ่มข้อมูลล้มเหลว",
                });
              },
            });
          }
        },
        error: function (xhr, status, error) {
          console.log("Error occurred during AJAX request:", error);
          Swal.fire({
            icon: "error",
            title: "เพิ่มข้อมูลล้มเหลว",
          });
        },
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "เพิ่มข้อมูลล้มเหลว",
        text: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
      });
    }
  });

  // R: Read
  var dataTable = $("#categoryList").DataTable({
    processing: true,
    ajax: {
      url: "./api/category/fetch_category.php",
      type: "POST",
    },
    scrollCollapse: true,
    scrollX: true,
    scrollY: "50vh",
    columns: [
      { data: "categoryId" },
      {
        data: "categoryImg",
        render: function (data, type, row) {
          return (
            '<img src="./upload_files/category/' + data + '" height="100">'
          );
        },
      },
      { data: "categoryName" },
      { data: "measure" },
      {
        data: "update",
        render: function (data, type, row) {
          return (
            '<button type="button" name="update" id="update-cate-' +
            data +
            '" class="btn btn-primary btn update">แก้ไข</button></button>'
          );
        },
      },
      {
        data: "delete",
        render: function (data, type, row) {
          return (
            '<button type="button" name="delete" id="delete-cate-' +
            data +
            '" class="btn btn-danger btn delete">ลบ</button>'
          );
        },
      },
    ],
    columnDefs: [{ orderable: false, targets: [1, 4, 5] }],
  });

  // U: Update
  $(document).on("click", ".update", function () {
    var m = $(this).attr("id");
    var manageId = m.toString();
    console.log("Manage ID:", manageId);
    $.ajax({
      url: "./api/category/single_category.php",
      method: "POST",
      data: { manageId: manageId },
      dataType: "json",
      success: function (data) {
        $("#modal-category").modal("show");
        $(".modal-title").text("แก้ไขหมวดหมู่อุปกรณ์");
        $("#categoryId").val(data.categoryId);
        $("#categoryId").attr("readonly", true);
        $("#categoryName").val(data.categoryName);
        $("#measure").val(data.measure);
        $("#categoryImgLabel").text(data.categoryImg);
        $("#uploadedCategoryImg").html(
          '<img src="./upload_files/category/' +
            data.categoryImg +
            '" height="150" class="rounded-2">'
        );
        $("#action").val("แก้ไข");
        $("#operation").val("update");

        $(document).on("submit", "#form_category", function (event) {
          event.preventDefault();
          var categoryId = $("#categoryId").val();
          $("#categoryId").attr("readonly", true);
          var categoryName = $("#categoryName").val();
          var measure = $("#measure").val();

          console.log(new FormData(this));

          if (categoryId != "" && categoryName != "" && measure != "") {
            $.ajax({
              url: "./api/category/manage_category.php",
              method: "POST",
              data: new FormData(this),
              contentType: false,
              processData: false,
              success: function (data) {
                console.log(data);
                $("#form_category")[0].reset();
                $("#modal-category .btn-close").click();
                Swal.fire({
                  icon: "success",
                  title: "ยืนยันการแก้ไขข้อมูล",
                  text: 'แก้ไข "' + categoryName + '" สำเร็จ',
                });
                dataTable.ajax.reload();
              },
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "แก้ไขข้อมูลล้มเหลว",
              text: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
            });
          }
        });
      },
    });
  });

  // D: Delete
  $(document).on("click", ".delete", function () {
    var m = $(this).attr("id");
    var manageId = m.toString();
    console.log("Manage ID:", manageId);
    $.ajax({
      url: "./api/category/single_category.php",
      method: "POST",
      data: { manageId: manageId },
      dataType: "json",
      success: function (data) {
        Swal.fire({
          title: "ยืนยันการลบข้อมูล",
          text: 'ต้องการลบ "' + data.categoryName + '" หรือไม่?',
          icon: "warning",
          showCancelButton: true,
          cancelButtonColor: "#d33",
          cancelButtonText: "ยกเลิก",
          confirmButtonColor: "#3085d6",
          confirmButtonText: "ยืนยัน",
        }).then(function (result) {
          var showName = data.categoryName;
          if (result.isConfirmed) {
            $.ajax({
              url: "./api/category/manage_category.php",
              method: "POST",
              data: { manageId: manageId, operation: "delete" },
              success: function (data) {
                Swal.fire({
                  title: "ยืนยันการลบข้อมูล",
                  text: 'ลบ "' + showName + '" สำเร็จ',
                  icon: "success",
                });
                dataTable.ajax.reload();
              },
            });
          }
        });
      },
    });
  });

});
